# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## Admin
| Commands                    | Arguments | Description                   |
| --------------------------- | --------- | ----------------------------- |
| disable                     | <none>    | Disable the bot               |
| enable                      | <none>    | Enable the bot                |
| toggleFunctionality, toggle | <none>    | Toggles the bot functionality |

## Utility
| Commands | Arguments | Description          |
| -------- | --------- | -------------------- |
| Help     | (Command) | Display a help menu. |

